# http4s service template

A Giter8 template for creating a service based upon several libraries
from the Scala ecosystem, mainly the tapir library and others:

- cats
- cats-effect
- circe
- doobie
- fs2
- http4s
- pureconfig
- tapir

This template is intended as a basis for HTTP services which includes endpoint
definitions via tapir and usually JSON serialisation.

## Usage

Just use the `new` command of sbt like this:

```
% sbt new https://codeberg.org/wegtam/http4s-tapir.g8.git
```

If you want to use the Scala-2 version then you have to specify the branch name:

```
% sbt new https://codeberg.org/wegtam/http4s-tapir.g8.git --branch scala-2
```

## Template license

Written in 2020 by Wegtam GmbH

To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this template to the public domain worldwide.
This template is distributed without any warranty. 

See http://creativecommons.org/publicdomain/zero/1.0/.
