addCompilerPlugin("org.scalameta" % "semanticdb-scalac" % "4.8.12" cross CrossVersion.full)
addSbtPlugin("com.github.sbt"    % "sbt-dynver"    % "5.0.1")
addSbtPlugin("de.heikoseeberger" % "sbt-header"    % "5.10.0")
addSbtPlugin("io.spray"          % "sbt-revolver"  % "0.10.0")
addSbtPlugin("ch.epfl.scala"     % "sbt-scalafix"  % "0.11.1")
addSbtPlugin("org.scalameta"     % "sbt-scalafmt"  % "2.5.2")
